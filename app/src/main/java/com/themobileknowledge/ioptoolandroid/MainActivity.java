package com.themobileknowledge.ioptoolandroid;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.media.Image;
import android.nfc.NfcAdapter;

import android.nfc.Tag;
import android.nfc.cardemulation.CardEmulation;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.themobileknowledge.ioptoolandroid.Service.Callback;
import com.themobileknowledge.ioptoolandroid.Service.HCEService;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class MainActivity extends Activity implements View.OnClickListener, Callback {

    private ImageView minus_button_top;
    private ImageView plus_button_top;
    private TextView text_top;
    private LinearLayout start_up;
    private ProgressBar progressbar;
    private CardEmulation cardEmulation;
    private ComponentName hostApduService;

    private String AID = "325041592E5359532E4444463031";

    private Activity activity;
    private NfcAdapter nfcAdapter;

    private int _xDelta;
    private int _yDelta;

    private ViewGroup upper_box;

    private TextView status_response;


    @Override
    public void response(String msg) {
        status_response.setText(msg);
    }

    private final class ChoiceTouchListener implements View.OnTouchListener{

        @Override
        public boolean onTouch(View v, MotionEvent event) {


            final int X = (int) event.getRawX();
            final int Y = (int) event.getRawY();
            RelativeLayout.LayoutParams layoutParams1 = (RelativeLayout.LayoutParams) v.getLayoutParams();

            switch (event.getAction() & MotionEvent.ACTION_MASK){
                case MotionEvent.ACTION_DOWN:
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) v.getLayoutParams();
                    _xDelta = X - layoutParams.leftMargin;
                    _yDelta = Y - layoutParams.topMargin;
                    break;
                case MotionEvent.ACTION_MOVE:
                    layoutParams1 = (RelativeLayout.LayoutParams) v.getLayoutParams();
                    layoutParams1.leftMargin = X - _xDelta;
                    layoutParams1.topMargin = Y - _yDelta;

                    layoutParams1.rightMargin =  -125;
                    layoutParams1.bottomMargin = -125;

                    break;
            }

            if( v.getId() == R.id.upper_box){
                upper_box.invalidate();
                v.setLayoutParams(layoutParams1);
            }

            return true;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Listen to NFC setting changes
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        HCEService.SetCallback(this);

        if (nfcAdapter == null) {
            // NFC is not available for device
            Toast.makeText(this, "NFC NOT AVAILABLE FOR DEVICE!", Toast.LENGTH_LONG).show();
        } else if (!nfcAdapter.isEnabled()) {
            // NFC is available for device but not enabled
            Intent intent = new Intent(Settings.ACTION_NFC_SETTINGS);
            startActivity(intent);
        } else {
            // NFC is enabled
            InitUiElements();
            this.activity = this;
            StartReaderMode();
        }

    }

    private void StartReaderMode() {
        int READER_FLAGS = NfcAdapter.FLAG_READER_NFC_V | NfcAdapter.FLAG_READER_NFC_B | NfcAdapter.FLAG_READER_NFC_A| NfcAdapter.FLAG_READER_NFC_A | NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK;
        Bundle options = new Bundle();
        options.putInt(NfcAdapter.EXTRA_READER_PRESENCE_CHECK_DELAY, 1);
        nfcAdapter.enableReaderMode(activity, new NfcAdapter.ReaderCallback() {
            @Override
            public void onTagDiscovered(Tag tag) {
                Log.d(TAG, "NFC onTagDiscovered" + tag.toString());
            }
        }, READER_FLAGS, options);
    }


    private void InitUiElements() {
        minus_button_top  = findViewById(R.id.minus_button_top);
        plus_button_top   = findViewById(R.id.plus_button_top);
        text_top          = findViewById(R.id.text_top);
        start_up          = findViewById(R.id.start_up);
        progressbar       = findViewById(R.id.progressbar);
        minus_button_top    .setOnClickListener(this);
        plus_button_top     .setOnClickListener(this);
        start_up            .setOnClickListener(this);
        status_response   = findViewById(R.id.status_response);
        upper_box = (RelativeLayout)findViewById(R.id.upper_box);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(1500, 1500);
        upper_box.setLayoutParams(layoutParams);
        upper_box.setOnTouchListener(new ChoiceTouchListener());
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void InitHCE() {
        CardEmulation cardEmulationManager = CardEmulation.getInstance(nfcAdapter);
        ComponentName paymentServiceComponent = new ComponentName(getApplicationContext(), HCEService.class.getCanonicalName());
        if (!cardEmulationManager.isDefaultServiceForCategory(paymentServiceComponent, CardEmulation.CATEGORY_PAYMENT)) {
            Intent intent = new Intent(CardEmulation.ACTION_CHANGE_DEFAULT);
            intent.putExtra(CardEmulation.EXTRA_CATEGORY, CardEmulation.CATEGORY_PAYMENT);
            intent.putExtra(CardEmulation.EXTRA_SERVICE_COMPONENT, paymentServiceComponent);
            startActivityForResult(intent, 0);
        }else{
            this.cardEmulation = CardEmulation.getInstance(nfcAdapter);
            this.hostApduService = new ComponentName(getApplicationContext(), HCEService.class);
            cardEmulationManager.setPreferredService(this, hostApduService);
            nfcAdapter.disableReaderMode(activity);
            registerAid();
            CheckServiceAIDs();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void EndHCE() {
        try{
            CardEmulation cardEmulationManager = CardEmulation.getInstance(nfcAdapter);
            ComponentName paymentServiceComponent = new ComponentName(getApplicationContext(), HCEService.class.getCanonicalName());
            if (cardEmulationManager.isDefaultServiceForCategory(paymentServiceComponent, CardEmulation.CATEGORY_PAYMENT)) {
                cardEmulationManager.unsetPreferredService(activity);
            }else{
                cardEmulationManager.unsetPreferredService(activity);
            }
            StartReaderMode();
        }catch (Exception e){
            Log.d(TAG, e.getLocalizedMessage());
        }
    }

    private void CheckServiceAIDs() {
        Log.i(TAG, "Check default for AID " +AID+": " + cardEmulation.isDefaultServiceForAid(hostApduService, AID));
        String aids = AID;
        if (aids != null) {
            Log.i(TAG, "Check default for AID from device " + aids + ": " + cardEmulation.isDefaultServiceForAid(hostApduService, aids));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Log.i(TAG, "Supports AID Prefix Registration: " + cardEmulation.supportsAidPrefixRegistration());
        }
    }

    private void registerAid() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (AID != null) {
                //Only register AID if not already registered
                if (!cardEmulation.isDefaultServiceForAid(hostApduService, AID)) {
                    List<String> aids = new ArrayList<>();
                    aids.add(AID);
                    boolean success = cardEmulation.registerAidsForService(hostApduService, CardEmulation.CATEGORY_PAYMENT, aids);
                    Log.i(TAG, "Dynamic AID registration return: " + success);
                } else {
                    Log.i(TAG, "AID already registered: " + AID + ": " + cardEmulation.isDefaultServiceForAid(hostApduService, AID));
                }
            }
        } else {
            Log.i(TAG, "AIDs can't be dynamically registered pre Lollipop");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.minus_button_top:
                MinusButtonLogic();
                break;
            case R.id.plus_button_top:
                PlusButtonLogic();
                break;
            case R.id.start_up:
                StartLogic();
                break;
        }
    }

    class StartTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object... params) {
            runOnUiThread(new Runnable() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    status_response.setText("Waiting status...");

                    Log.d(TAG, "Pressed START");
                    progressbar.setVisibility(View.VISIBLE);
                    InitHCE();

                    Log.d(TAG, "Started Service");
                    String inputTimeStr = text_top.getText().toString();
                    double tempval = Double.valueOf(inputTimeStr);

                    //0.1 == 100
                    int timeToDoTask = (int) (tempval * 1000);

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "Stopped Service");
                            progressbar.setVisibility(View.INVISIBLE);
                            EndHCE();
                        }
                    }, timeToDoTask);
                }
            });
            return null;
        }
    }

    private void StartLogic() {
        StartTask task = new StartTask();
        task.execute();
    }

    private void PlusButtonLogic() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String inputTimeStr = text_top.getText().toString();
                double tempval = Double.valueOf(inputTimeStr);
                if(tempval == (double) 2.0){ return; }
                int tempInt = (int) ((tempval * 10) + 1);
                double inputTimeValue   = tempInt / 10.0;
                String valueToChangeStr = String.valueOf(inputTimeValue);
                text_top  .setText(valueToChangeStr);
            }
        });
    }

    private void MinusButtonLogic() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String inputTimeStr = text_top.getText().toString();
                double tempval = Double.valueOf(inputTimeStr);

                if(tempval == (double) 0.0){
                    return;
                }

                int tempInt = (int) ((tempval * 10) - 1);
                double inputTimeValue = tempInt / 10.0;

                String valueToChangeStr = String.valueOf(inputTimeValue);
                text_top.setText(valueToChangeStr);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}

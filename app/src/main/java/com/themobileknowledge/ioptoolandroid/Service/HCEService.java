package com.themobileknowledge.ioptoolandroid.Service;

import android.nfc.cardemulation.HostApduService;
import android.os.Bundle;
import android.telecom.Call;
import android.util.Log;
import android.widget.Toast;

import java.util.Random;

import static android.content.ContentValues.TAG;

public class HCEService extends HostApduService {

    static Callback callbackService;

    public static void SetCallback(Callback callback){
        callbackService = callback;
    }

    @Override
    public byte[] processCommandApdu(byte[] apdu, Bundle extras) {
        if (selectAidApdu(apdu)) {
            /** STEP 1-2 **/
            Log.i(TAG, "Application selected");
            byte [] array = new byte[]{(byte) 0x00, (byte) 0xB2, (byte) 0x01, (byte) 0x0C, (byte) 0x00, (byte) 0x90, (byte) 0x00};
            Log.i(TAG, "Response Select AID: "+byteArrayToHex(array) );
            return array;
        } else {
            /** STEP 3-4 **/
            if(apdu[0] == (byte)0x00 && apdu[1] == (byte)0xB2 && apdu[2] == (byte)0x01 && apdu[3] == (byte)0x0C ) {
                byte [] dataToSend = new byte[256];
                Random random = new Random(); random.nextBytes(dataToSend);
                dataToSend[0] = (byte) 0x00;   dataToSend[1] = (byte) 0x04;     dataToSend[2] = (byte) 0x04;
                dataToSend[3] = (byte) 0x00;   dataToSend[4] = (byte) 0xF8;     dataToSend[5] = (byte) 0x01;
                dataToSend[6] = (byte) 0x02; dataToSend[252] = (byte) 0xF8;   dataToSend[253] = (byte) 0x00;
                dataToSend[254] = (byte) 0x90; dataToSend[255] = (byte) 0x00;
                Log.i(TAG, "Response Read Record 1: "+byteArrayToHex(dataToSend) );
                return dataToSend;
            }
            if(apdu[0] == (byte) 0x00 && apdu[1]==(byte)0x04 && apdu[2]==(byte)0x04 && apdu[3] == (byte)0x00 && apdu[4]==(byte)0xF8) {
                /** STEP 5-6 **/
                byte [] array = new byte[]{(byte) 0x80, (byte) 0xA8, (byte) 0x00, (byte) 0x00, (byte) 0x04,
                        (byte) 0xFF,  (byte) 0xFE, (byte) 0x00, (byte) 0x64,(byte) 0x00, (byte) 0x90, (byte) 0x00};
                Log.i(TAG, "Response INS 4 command: "+byteArrayToHex(array) );

                return array;
            }

            if(apdu[0] == (byte) 0x80 && apdu[1] == (byte) 0xA8 && apdu[2] == (byte) 0x00 && apdu[3] == (byte) 0x00 && apdu[4] == (byte) 0x04) {
                /** STEP 7-8 **/
                byte [] array = new byte[]{(byte) 0x00, (byte) 0xB2,  (byte) 0x02, (byte) 0x0C, (byte) 0x00, (byte) 0x90, (byte) 0x00 };
                Log.i(TAG, "Response to GPO command: "+byteArrayToHex(array) );

                return array;
            }

            if(apdu[0] == (byte)0x00 && apdu[1] == (byte)0xB2 && apdu[2] == (byte)0x02 && apdu[3] == (byte)0x0C && apdu[4] == (byte)0x00)
            {
                /** STEP 9-10 **/
                byte [] array = new byte[]{(byte) 0x00, (byte) 0x70,  (byte) 0x02, (byte) 0x04, (byte) 0x00, (byte) 0x90, (byte) 0x00};
                Log.i(TAG, "Response EOT Command: "+byteArrayToHex(array) );
                callbackService.response("IOP Transmission Status: OK");
                return array;
            }
            Log.i(TAG, "Error");
            return new byte[]{(byte) 0x6A, (byte)0x82};
        }
    }

    private boolean selectAidApdu(byte[] apdu) {
        return apdu.length >= 2 && apdu[0] == (byte)0 && apdu[1] == (byte)0xa4;
    }

    @Override
    public void onDeactivated(int reason) {
        Log.i(TAG, "Deactivated: " + reason);
    }

    public static String byteArrayToHex(byte[] a) {
        try{
            StringBuilder sb = new StringBuilder(a.length * 2);
            for(byte b: a)
                sb.append(String.format("%02x", b));
            return sb.toString();
        }catch (Exception e){
            return "ERROR";
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}